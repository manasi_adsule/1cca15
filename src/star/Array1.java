package star;

import javax.xml.bind.SchemaOutputResolver;

public class Array1 {
    public static void main(String[] args) {
        int [] data;  // declaration
        data=new int[5]; //size allocation
        //initialization
        data[0]= 100;
        data[1]= 200;
        data[2]= 300;
        data[3]= 400;
        data[4]= 500;
        // using for loop to print all data in single print statement.
            for(int i=0; i<=4; i++)
            {
                System.out.println(data[i]);
            }
    }
}
