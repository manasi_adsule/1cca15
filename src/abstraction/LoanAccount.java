package abstraction;
//step-2
public class LoanAccount implements Account {
    double loanAmount;

    public LoanAccount(double loanAmount) {
        this.loanAmount = loanAmount;
        System.out.println("loan account created.");
    }

    @Override
    public void deposit(double amt) {
        loanAmount-=amt;
        System.out.println(amt+"Rs debited from loan account");
    }

    @Override
    public void withdraw(double amt) {
        loanAmount+=amt;
        System.out.println(amt+"Rs credited to ur account");
    }

    @Override
    public void checkBalance() {
        System.out.println("active loan amount"+loanAmount);
    }
}
