package abstraction;

import java.util.Scanner;

//utilization layer
public class MainApp {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Select account type:");
        System.out.println("1: Saving \n 2: Loan");
        int accType = sc.nextInt();
        System.out.println("Enter account opening balance:");
        double balance = sc.nextDouble();
        // object creation
        AccountFactory factory = new AccountFactory();
        Account accRef = factory.createAccount(accType, balance);
        boolean status = true;
        while (status) {
            System.out.println("select mode of transaction:");
            System.out.println("1: Deposit \n 2: Withdraw \n 3: Check balance \n 4: Exit");
            int choice = sc.nextInt();
            if (choice == 1) {
                System.out.println("Enter amount");
                double amt = sc.nextDouble();
                accRef.deposit(amt);
            } else if (choice == 2) {
                System.out.println("Enter amount");
                double amt = sc.nextDouble();
                accRef.withdraw(amt);
            } else if (choice == 3) {
                accRef.checkBalance();
            } else {
                status = false;
            }
        }
    }
}