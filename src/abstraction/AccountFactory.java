package abstraction;
//factory class
public class AccountFactory {
    //factory method
    Account createAccount(int type, double balance){
        Account a1=null;
        if(type==1) {
            a1 = new SavingAccount(balance);  //upcasting
        }
        else if(type==2) {
            a1 = new LoanAccount(balance);    //upcasting
        }
            return a1;
       }
}
