package abstraction;
//step-2
public class SavingAccount implements Account{
    double accountBalance;

    public SavingAccount(double accountBalance) {
        this.accountBalance = accountBalance;
        System.out.println("Saving account created");
    }

    @Override
    public void deposit(double amt) {
        accountBalance+=amt;
        System.out.println(amt+ "Rs credited to ur account");
    }

    @Override
    public void withdraw(double amt) {
        if(amt<=accountBalance){
            accountBalance-=amt;
            System.out.println(amt+ "Rs debited from ur account");
        }else{
            System.out.println("insufficient balance");
        }
    }

    @Override
    public void checkBalance() {
        System.out.println("active balance:"+accountBalance);
    }
}
