package numberpattern;

import javax.xml.bind.SchemaOutputResolver;

public class Number1 {
    public static void main(String[] args) {
        int line=5;
        int star=5;
        int ch=1;
        for (int i=0; i<line; i++){

            for (int j=0; j<star; j++){
                System.out.print(ch);
                ch+=1;
            }
            System.out.println();
        }
    }
}
